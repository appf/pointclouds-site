#!/bin/bash
source .env

if [[ $# -eq 0 ]] ; then
    echo 'usage ./upload.sh <potree pointcloud directory>'
    exit 0
fi

./b2-linux authorize-account "$B2_ACCOUNT" "$B2_KEY"

bn=`basename $1`
./b2-linux sync --threads 64 --skipNewer $1 "$B2_BUCKET_PATH/$bn"