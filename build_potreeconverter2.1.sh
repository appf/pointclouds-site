#!/bin/bash
pushd .
rm -rf build_PotreeConverter
mkdir build_PotreeConverter
cd build_PotreeConverter
rm 2.1.zip
wget https://github.com/potree/PotreeConverter/archive/refs/tags/2.1.zip
unzip 2.1.zip
cd PotreeConverter-2.1/
mkdir build
cd build
cmake ../
make -j12
cp PotreeConverter ../../../PotreeConverter2.1
popd
rm -rf build_PotreeConverter

