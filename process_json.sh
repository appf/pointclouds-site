#!/bin/bash
source .env

./b2-linux authorize-account "$B2_ACCOUNT" "$B2_KEY"

while read -r obj; do
    filepath=output/$(echo "$obj" | jq -r ".download_url")
    name=$(echo "$obj" | jq -r ".title" | sed 's/ /_/g' | sed 's/_-_/_/g'|sed 's/\[//g'| sed 's/\]//g'| sed 's/\///g')
    if [[ -f $filepath ]]; then
        ./PotreeConverter2.1 "$filepath" -o "done/$name"
        pt2succ=$?
        ./PotreeConverter1.6 "$filepath" -o "done/$name"
        pt1succ=$?
        if [[ $pt2succ -eq 0  || $pt1succ -eq 0 ]]; then
            echo "---" > "site/content/misc/$name.md"
            echo "$(echo "$obj" |jq '.' | yq eval -P)" >> "site/content/misc/$name.md"
            echo "potree:" >> "site/content/misc/$name.md"
            if [[ $pt1succ -eq 0 ]]; then
                echo "    url: $PUBLIC_URL_PREFIX/$name/cloud.js" >> "site/content/misc/$name.md"
            fi
            if [[ $pt2succ -eq 0 ]]; then  
                echo "    url_v2: $PUBLIC_URL_PREFIX/$name/metadata.json" >> "site/content/misc/$name.md"  
            fi
            echo "---" >> "site/content/misc/$name.md"
        fi
        
        fp=`basename $filepath`
        fileext=${fp##*.}
        cp "$filepath" "done/$name/original.$fileext"

        
        echo "[PROCESSED] $name"
        echo "[UPLOADING]" $name
        ./b2-linux sync --threads 64 --skipNewer "done/$name" "$B2_BUCKET_PATH/$name"
        echo "[UPLOADED] $name"
    else
        echo "[SKIPPED] $filepath does not exist"
    fi
done < <(jq -c '.[]' processme.json )