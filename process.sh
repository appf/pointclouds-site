#!/bin/bash
if [[ ! -f .env ]]; then
    echo "you need an env file with B2_ACCOUNT, B2_KEY and PUBLIC_URL_PREFIX"
    exit 0
fi

source .env

if [[ $# -eq 0 ]] ; then
    echo 'processes and uploads point clouds'
    echo 'usage ./process.sh <file> <section (optional)>'
    exit 0
fi

SECTION=${2:-misc}

./b2-linux authorize-account "$B2_ACCOUNT" "$B2_KEY"
export PUBLIC_URL_PREFIX="${PUBLIC_URL_PREFIX}"
FILEPATH=`readlink -f "$1"`
FILENAME=`basename $FILEPATH`
EXT=${FILENAME##*.}
FILENAME_BASE="${FILENAME%.*}"
NAME=$(echo "$FILENAME_BASE" | \
    sed 's/ /_/g' | \
    sed 's/_-_/_/g' | \
    sed 's/\[//g' | \
    sed 's/\]//g' | \
    sed 's/\///g'| \
    sed 's/\+//g')

set -xe

if [[ -f $FILEPATH ]]; then
    echo "[POTREE2.1] ..."
    ./PotreeConverter2.1 "$FILEPATH" -o "done/$NAME"
    pt2succ=$?
    echo "[POTREE1.6] ..."
    ./PotreeConverter1.6 "$FILEPATH" -o "done/$NAME"
    pt1succ=$?
    if [[ $pt2succ -eq 0  || $pt1succ -eq 0 ]]; then
        echo "[PROCESSED] $NAME"
        cp "$FILEPATH" "done/$NAME/original.$EXT"
        echo "[UPLOADING] $NAME"
        ./b2-linux sync --threads 64 --skipNewer "done/$NAME" "$B2_BUCKET_PATH/$NAME"
        echo "[UPLOADED] $NAME"
        pushd site
        hugo new "$SECTION/$NAME.md"
        popd
    fi
    xdg-open "site/content/$SECTION/$NAME.md"
else
    echo "[SKIPPED] $FILEPATH does not exist"
fi