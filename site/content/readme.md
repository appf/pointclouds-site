---
hidden: true
---

# Available settings for frontmatter


Example with all options:

```yaml
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Kioloa_Coastline_2018-03-10/cloud.js
    settings:
        pointbudget: 1000000
        fov: 70
        usehq: true
        opacity: 1
        edlenabled: true
        edlradius: 1.4
        edlstrength: 0.4
        background: gradient
        minnodesize: 30
        showboundingbox
        position: 262960.419, 6063383.889, -120.203
        target: 262975.258, 6063483.024, -130.929
        material:
            activeattribute: elevation
            minsize: 1.1
            pointsizetype: ADAPTIVE
            pointshape: CIRCLE

```

if you use `usehq` then pointshape will change to CIRCLE