---
title: Kioloa Forest Edge
summary: Forest edge on the ANU Kioloa Coastal Campus, NSW, Australia
draft: false
files_local_url: ef4ec18b87bcb6f2a09dcaaed552f0ca.laz
download_url: 1583970465.0219471_odm_georeferenced_model.laz
timestamp: "2020-03-11T23:47:53.432Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/kioloa_forest_edge_flight/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/kioloa_forest_edge_flight/metadata.json
---
