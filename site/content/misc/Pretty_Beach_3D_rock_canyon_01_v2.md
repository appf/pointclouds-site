---
title: Pretty Beach 3D rock canyon 01 v2
summary: Description
draft: false
hidden: true
location: Pretty Beach
files_local_url: 78ea9f014c8fb7b35a9290ab161aec072017-09-25_Pretty_Beach_Rock_Canyon_1_Cropped.ply
download_url: 78ea9f014c8fb7b35a9290ab161aec072017-09-25_Pretty_Beach_Rock_Canyon_1_Cropped.ply
timestamp: "2017-09-07T04:21:25.342Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Pretty_Beach_3D_rock_canyon_01_v2/cloud.js
---
