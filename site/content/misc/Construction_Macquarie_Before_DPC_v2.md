---
title: Construction_Macquarie_Before_DPC v2
summary: Trevor point cloud
draft: false
hidden: true
files_local_url: bc0914f304bdd7600466958940fc523dConstruction_Macquarie_Before_DPC.las
download_url: bc0914f304bdd7600466958940fc523dConstruction_Macquarie_Before_DPC.las
timestamp: "2017-10-25T14:37:55.646Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Construction_Macquarie_Before_DPC_v2/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/Construction_Macquarie_Before_DPC_v2/metadata.json
---
