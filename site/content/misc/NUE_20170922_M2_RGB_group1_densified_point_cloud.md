---
title: NUE_20170922_M2_RGB_group1_densified_point_cloud
summary: TropAg test
draft: false
hidden: true
files_local_url: 23f451d37118893fd4a7e96a3872fd11NUE_20170922_M2_RGB_group1_densified_point_cloud.laz
download_url: 23f451d37118893fd4a7e96a3872fd11NUE_20170922_M2_RGB_group1_densified_point_cloud.laz
timestamp: "2017-10-25T14:37:55.646Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/NUE_20170922_M2_RGB_group1_densified_point_cloud/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/NUE_20170922_M2_RGB_group1_densified_point_cloud/metadata.json
---
