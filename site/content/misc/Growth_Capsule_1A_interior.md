---
title: Growth Capsule 1A interior
summary: Sample scan of the inside of an APPF Growth Capsule
draft: false
files_local_url: 8178e13fa7631716ae6d3c6797cc997a.laz
download_url: 1583468490.0168192_8_capsule_int_1a.laz
timestamp: "2020-03-06T04:21:42.538Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Growth_Capsule_1A_interior/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/Growth_Capsule_1A_interior/metadata.json
---

Note: you may need to zoom in quite a bit to see the inside of the capsule
