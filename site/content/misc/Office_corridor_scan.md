---
title: Office corridor scan
summary: Description
draft: false
hidden: true
files_local_url: f12d3556803bf018756aaeeaaf5ef68f.laz
download_url: 1583798738.3743367_0_ruariadh_test.laz
timestamp: "2020-03-10T00:10:20.130Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Office_corridor_scan/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/Office_corridor_scan/metadata.json
---
