---
title: Sorghum Root reconstruction - Topp lab
summary: Fine 3D scan of Sorghum roots, from the Topp Lab
draft: false
files_local_url: d41d8cd98f00b204e9800998ecf8427e1_6thattempt-test1-3ptmatches_group1_densified_cropped.ply
download_url: d41d8cd98f00b204e9800998ecf8427e1_6thattempt-test1-3ptmatches_group1_densified_cropped.ply
timestamp: "2018-07-14T03:26:37.175Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Sorghum_Root_reconstruction_Topp_lab_v1/cloud.js
---
