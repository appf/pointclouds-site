---
title: Pagan Farm and animal rescue, NSW
summary: Description
draft: false
hidden: true
location: -35.216845, 149.238876
files_local_url: 87e620d328b5c2d70d8d6a8d152ad84b2017-10-05_Joanna_Pagan_farm_NSW-densified_point_cloud.las
download_url: 87e620d328b5c2d70d8d6a8d152ad84b2017-10-05_Joanna_Pagan_farm_NSW-densified_point_cloud.las
timestamp: "2017-10-04T15:23:43.726Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Pagan_Farm_and_animal_rescue,_NSW/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/Pagan_Farm_and_animal_rescue,_NSW/metadata.json
---
