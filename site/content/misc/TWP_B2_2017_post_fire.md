---
title: TWP_B2_2017_post_fire
summary: |-
  TWP_B2_2017_post_fire
  Data from Shaun Levick
draft: false
hidden: true
files_local_url: e36a0b13cf25670304c25a0484d4c65fTWP_B2_2017_post_fire.laz
download_url: e36a0b13cf25670304c25a0484d4c65fTWP_B2_2017_post_fire.laz
timestamp: "2017-11-06T11:15:20.245Z"
display_material: height
display_use_edl: true
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/TWP_B2_2017_post_fire/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/TWP_B2_2017_post_fire/metadata.json
---
