---
title: Tobacco plant test 1
summary: Description
draft: false
hidden: true
files_local_url: 9e073b38b27f083840f94d55284d20b3.las
download_url: 1596093268.3571513_2020-07-30-GC05-PlantsLeftHandShot-60im-v1_group1_densified_point_cloud.las
timestamp: "2020-07-30T07:14:59.968Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Tobacco_plant_test_1/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/Tobacco_plant_test_1/metadata.json
---
