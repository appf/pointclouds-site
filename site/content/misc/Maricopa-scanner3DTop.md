---
title: Maricopa-scanner3DTop
summary: Description
draft: false
hidden: true
files_local_url: d41d8cd98f00b204e9800998ecf8427escanner3DTop_L1_ua-mac_2017-03-07__13-23-53-546_merged.las
download_url: d41d8cd98f00b204e9800998ecf8427escanner3DTop_L1_ua-mac_2017-03-07__13-23-53-546_merged.las
timestamp: "2017-12-11T17:26:35.797Z"
display_material: height
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Maricopa-scanner3DTop/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/Maricopa-scanner3DTop/metadata.json
---
