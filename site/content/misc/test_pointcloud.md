---
title: test pointcloud
summary: Description
access: 4
hidden: true
files_local_url: 4f667cdcdd3bc2491e8b1858b7b82666.las
download_url: 1569302528.933659_las.las
timestamp: "2019-09-24T05:22:26.592Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/test_pointcloud/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/test_pointcloud/metadata.json
---
