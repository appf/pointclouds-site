---
title: NCRIS PGF + tray scan room
summary: Internal lab area scan of the NCRIS Plant Growth Facility and APPF wet lab in the Linnaeus building, using Zeb Horizon LIDAR
draft: false
files_local_url: 961e9907c1198f45ac5f7dbd99ad7eae.laz
download_url: 1583466115.1116014_4_ncris-pgf-appf-wetlab.laz
timestamp: "2020-03-06T03:44:05.415Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/NCRIS_PGF_tray_scan_room/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/NCRIS_PGF_tray_scan_room/metadata.json
---
