---
title: Field trip dieback site scan w/ Cris Brack
summary: Site of a Fenner field trip investigating possible causes of Eucalypt dieback in Berridale, NSW
draft: false
location: Berridale, NSW, Australia
files_local_url: 06d4f85af3a1587b8a1d4f8d994a6696.ply
download_url: 1571027529.9545155_fieldtrip_group1_densified_point_cloud.ply
timestamp: "2019-10-14T04:39:53.304Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Field_trip_dieback_site_scan_w_Cris_Brack/cloud.js
---
