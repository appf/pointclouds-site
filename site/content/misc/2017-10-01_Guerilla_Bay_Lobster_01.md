---
title: 2017-10-01 Guerilla Bay Lobster 01
summary: Description
draft: false
hidden: true
files_local_url: c5f5bb1a5220bc14dd6a29a988351666Lobster-184863_group1_densified_point_cloud.las
download_url: c5f5bb1a5220bc14dd6a29a988351666Lobster-184863_group1_densified_point_cloud.las
timestamp: "2017-10-04T15:23:43.726Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/2017-10-01_Guerilla_Bay_Lobster_01/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/2017-10-01_Guerilla_Bay_Lobster_01/metadata.json
---
