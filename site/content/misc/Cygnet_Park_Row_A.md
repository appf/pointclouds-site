---
title: Cygnet Park Row A
summary: Description
draft: false
hidden: true
files_local_url: ef54ea2bb79bc98e28a9da2e4f965259.las
download_url: 1583870465.2155645_0128Q6_CPall_LN_dn_s4_i5_SA2_LGN_town_ngc5_ch_LC_go2_sb_st_wg_ko_RGB_cutto_CPplus_A1_MERGED_2020-03-11_01h53_10_087.las
timestamp: "2020-03-10T20:06:35.462Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Cygnet_Park_Row_A/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/Cygnet_Park_Row_A/metadata.json
---
