---
title: 2017-09-25 Pretty Beach Rock Canyon 2
summary: Description
draft: false
hidden: true
files_local_url: cba5b047f154f7e77280333ab94e0f8f2017-09-25_Pretty_Beach_Rock_Canyon_2_group1_densified_point_cloud.las
download_url: cba5b047f154f7e77280333ab94e0f8f2017-09-25_Pretty_Beach_Rock_Canyon_2_group1_densified_point_cloud.las
timestamp: "2017-09-07T04:21:25.342Z"
display_use_edl: false
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/2017-09-25_Pretty_Beach_Rock_Canyon_2/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/2017-09-25_Pretty_Beach_Rock_Canyon_2/metadata.json
---
