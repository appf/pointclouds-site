---
title: Kioloa Coastline
summary: Beach near the ANU Kioloa Coastal Campus, NSW, Australia
draft: false
location: Kioloa Coastal Campus, NSW, Australia
files_local_url: d41d8cd98f00b204e9800998ecf8427e2018-03-10-Kioloa-Coastline-R1-R2_group1_densified_point_cloud.las
download_url: d41d8cd98f00b204e9800998ecf8427e2018-03-10-Kioloa-Coastline-R1-R2_group1_densified_point_cloud.las
timestamp: "2018-03-14T18:21:46.582Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Kioloa_Coastline_2018-03-10/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/Kioloa_Coastline_2018-03-10/metadata.json
    settings:
        background: skybox
        usehq: true
        edlenabled: true
        position: 262960.419, 6063383.889, -120.203
        target: 262975.258, 6063483.024, -130.929

---
Beach near the ANU Kioloa Coastal Campus, NSW, Australia
