---
title: Square Rock
summary: Square Rock site for the Australian Mountain Research Facility (AMRF) project
draft: false
files_local_url: 7d7f012bc0b79da890f765343e074a8c.las
download_url: 1569291309.602804_Square-Rock-AMRF_point_cloud.las
timestamp: "2019-09-24T02:20:31.532Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Square-Rock-AMRF/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/Square-Rock-AMRF/metadata.json
---
