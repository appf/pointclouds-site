---
title: 'Esdale 4 year old tree planting '
summary: Description
draft: false
hidden: true
location: Esdale, NSW, Australia
files_local_url: dceccf9e78e0e1aac053e0cce91cf11d.ply
download_url: 1571014482.5341692_craig-esdale_group1_densified_point_cloud.ply
timestamp: "2019-10-14T01:01:14.331Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Esdale_4_year_old_tree_planting_/cloud.js
---
