---
title: Linnaeus Building Interior
summary: Internal office area scan of the Linnaeus building using Zeb Horizon LIDAR
draft: false
files_local_url: 86aae41faee6aa082f82db1ebc66496a.laz
download_url: 1583468880.9371538_5_linnaeus_3floors.laz
timestamp: "2020-03-06T04:33:10.497Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Linnaeus_interior/cloud.js
    material:
        activeattribute: elevation
---
