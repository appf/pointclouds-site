---
title: Swanton-Morley 2019-06-28
summary: Description
draft: false
hidden: true
location: Norfolk, UK
files_local_url: 8dda4561ceae469f47ff59c9c6fe2d46.las
download_url: 1582690319.0591376_Merged.las
timestamp: "2020-02-26T04:14:08.766Z"
display_material: rgb
display_navigation: orbit
display_use_edl: true
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Swanton-Morley_2019-06-28/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/Swanton-Morley_2019-06-28/metadata.json
---
