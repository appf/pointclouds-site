---
title: Leo Class Mars model [2018-09-14]
summary: Description
draft: false
hidden: true
files_local_url: d41d8cd98f00b204e9800998ecf8427eLeo_class_mars_model_2018-09-13_group1_densified_point_cloud.las
download_url: d41d8cd98f00b204e9800998ecf8427eLeo_class_mars_model_2018-09-13_group1_densified_point_cloud.las
timestamp: "2018-09-14T13:03:49.917Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Leo_Class_Mars_model_2018-09-14/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/Leo_Class_Mars_model_2018-09-14/metadata.json
---
