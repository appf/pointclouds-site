---
title: test
summary: Description
draft: false
hidden: true
files_local_url: c24a8dcfbca1bce08c9c52185220f9fd.las
download_url: 1571626006.1228132_2019-10-18-acton-peninsula-merged.las
timestamp: "2019-11-14T04:44:46.199Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/test/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/test/metadata.json
---
