---
title: ANU Forest - National Arboretum - ProUAV [2017-06-25] - r01
summary: Description
draft: false
hidden: true
location: National Arboretum, Canberra, ACT, Australia
files_local_url: 6912b4982373bf2b3bd09fdaf0a99cbe2017-06-25-nac-anuf-prouav-f01-r01_group1_densified_point_cloud.ply
download_url: 6912b4982373bf2b3bd09fdaf0a99cbe2017-06-25-nac-anuf-prouav-f01-r01_group1_densified_point_cloud.ply
timestamp: "2017-09-07T02:58:38.710Z"
spp: Corymbia maculata (spotted gum), Eucalyptus tricarpa (red ironbark)
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/National_Arboretum_ANU_Forest_ProUAV_2017-06-25_r01/cloud.js
---
