---
title: National Arboretum - Full Arboretum (front) - ProUAV [2017-03-16]
summary: Description
draft: false
hidden: true
location: National Arboretum, Canberra, ACT, Australia
spp: ""
files_local_url: 819a88bb4081f06542b63acff7c1aab32017-03-16-arboretum-rgb-full-ALL.ply
download_url: 819a88bb4081f06542b63acff7c1aab32017-03-16-arboretum-rgb-full-ALL.ply
timestamp: "2017-04-19T07:27:51.522Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/National_Arboretum_Full_Arboretum_front_ProUAV_2017-03-16/cloud.js
---
