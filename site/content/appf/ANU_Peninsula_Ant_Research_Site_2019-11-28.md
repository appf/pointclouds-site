---
title: ANU Peninsula Ant Research Site [2019-11-28]
summary: Description
draft: false
hidden: true
files_local_url: 41472241c82902b48c344642be6df15f.ply
download_url: 1574904774.8854105_acton-peninsula2_group1_densified_point_cloud.ply
timestamp: "2019-11-28T01:42:00.310Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/ANU_Peninsula_Ant_Research_Site_2019-11-28/cloud.js
---
