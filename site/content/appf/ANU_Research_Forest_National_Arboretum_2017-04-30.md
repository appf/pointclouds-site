---
title: National Arboretum - ANU Research Forest [2017-04-30]
summary: Scan of the ANU research forest at the National Arboretum
draft: false
location: National Arboretum, Canberra, ACT, Australia
spp: ""
files_local_url: bd4cdbbf39c5923c6a8aafde02f385c52017-04-30-nac-anuf-tim-f01-r01_group1_densified_point_cloud.ply
download_url: bd4cdbbf39c5923c6a8aafde02f385c52017-04-30-nac-anuf-tim-f01-r01_group1_densified_point_cloud.ply
timestamp: "2017-04-19T07:27:51.522Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/National_Arboretum_ANU_Research_Forest_2017-04-30/cloud.js
---
