---
title: National Arboretum - ANU Research Forest [2015-03-11]
summary: Fall scan of the ANU research forest at the National Arboretum
draft: false
location: National Arboretum, Canberra, ACT, Australia
spp: ""
files_local_url: 2583cb3211968fa6a1ee31bbc2eb72a22015-03-11-nac-anuf-tim-fm007_fm008-r01_group1_densified_point_cloud.ply
download_url: 2583cb3211968fa6a1ee31bbc2eb72a22015-03-11-nac-anuf-tim-fm007_fm008-r01_group1_densified_point_cloud.ply
timestamp: "2017-04-19T07:27:51.522Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/ANU_Research_Forest_National_Arboretum_2015-03-11/cloud.js
---
