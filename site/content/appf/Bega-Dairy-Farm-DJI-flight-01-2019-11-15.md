---
title: Bega-Dairy-Farm-DJI-flight-01-[2019-11-15]
summary: Description
draft: false
hidden: true
files_local_url: d595bc88129293cee0e5c801eb2bfbc6.las
download_url: 1574735728.2159271_Bega-Dairy-farm-DJI-01-[2019-11-15].las
timestamp: "2019-11-26T02:36:37.757Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Bega-Dairy-Farm-DJI-flight-01-2019-11-15/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/Bega-Dairy-Farm-DJI-flight-01-2019-11-15/metadata.json
---
