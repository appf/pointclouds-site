---
title: National Arboretum - ANU Research Forest [2017-09-11]
summary: Scan of the ANU research forest at the National Arboretum, by ProUAV at 60/80m altitudes
draft: false
location: National Arboretum, Canberra, ACT, Australia
spp: Corymbia maculata (spotted gum), Eucalyptus tricarpa (red ironbark)
files_local_url: 115dd5d8dc8847d3450475a99aea582b2017-09-11-nac-anuf-prouav-f60m-f80m-r02_group1_densified_point_cloud.ply
download_url: 115dd5d8dc8847d3450475a99aea582b2017-09-11-nac-anuf-prouav-f60m-f80m-r02_group1_densified_point_cloud.ply
timestamp: "2017-09-07T04:21:25.342Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/National_Arboretum_ANU_Forest_ProUAV_60_80m_r02_2017-09-11/cloud.js
---
Taken by ProUAV at flight altitudes of 60m and 80m.
