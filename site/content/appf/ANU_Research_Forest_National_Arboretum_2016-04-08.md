---
title: National Arboretum - ANU Research Forest [2016-04-08]
summary: Scan of the ANU research forest at the National Arboretum
draft: false
hidden: true
location: National Arboretum, Canberra, ACT, Australia
spp: ""
files_local_url: 63fc6f6aa6b30e3ba4a9a845b4e9f8f12016-04-08-nac-anuf-prouav-f01-r01_group1_densified_point_cloud.ply
download_url: 63fc6f6aa6b30e3ba4a9a845b4e9f8f12016-04-08-nac-anuf-prouav-f01-r01_group1_densified_point_cloud.ply
timestamp: "2017-04-19T07:27:51.522Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/ANU_Research_Forest_National_Arboretum_2016-04-08/cloud.js
---
