---
title: National Arboretum - ANU Research Forest [2016-08-14]
summary: Scan of the ANU research forest at the National Arboretum
draft: false
location: National Arboretum, Canberra, ACT, Australia
spp: ""
files_local_url: e6eaae99dd7f137006013b391b21afe02016-08-14-nac-anuf-prouav-f01-f02-r01_group1_densified_point_cloud.ply
download_url: e6eaae99dd7f137006013b391b21afe02016-08-14-nac-anuf-prouav-f01-f02-r01_group1_densified_point_cloud.ply
timestamp: "2017-04-19T07:27:51.522Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/ANU_Research_Forest_National_Arboretum_2016-08-14/cloud.js
---
