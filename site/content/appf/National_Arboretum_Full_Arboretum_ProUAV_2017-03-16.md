---
title: National Arboretum - Full Arboretum [2017-03-16]
summary: Full scan of the National Arboretum, Canberra, taken by ProUAV for the APPF
draft: false
location: National Arboretum, Canberra, ACT, Australia
spp: ""
files_local_url: 618f1ff505ffe2856850142b8f2d4fed2017-03-16-nac-all-abcdefghi-pix4d-r01-merge_group1_densified_point_cloud.laz
download_url: 618f1ff505ffe2856850142b8f2d4fed2017-03-16-nac-all-abcdefghi-pix4d-r01-merge_group1_densified_point_cloud.laz
timestamp: "2017-04-19T07:27:51.522Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/National_Arboretum_Full_Arboretum_ProUAV_2017-03-16/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/National_Arboretum_Full_Arboretum_ProUAV_2017-03-16/metadata.json
---
