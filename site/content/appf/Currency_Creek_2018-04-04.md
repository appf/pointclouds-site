---
title: Currency Creek Arboretum [2018-04-04]
summary: Scan of the Currency Creek Arboretum during the Borevitz Lab sampling field trip
draft: false
location: Currency Creek
files_local_url: d41d8cd98f00b204e9800998ecf8427e2018-04-04-currencycreek-all-ming_group1_densified_point_cloud.las
download_url: d41d8cd98f00b204e9800998ecf8427e2018-04-04-currencycreek-all-ming_group1_densified_point_cloud.las
timestamp: "2018-04-17T14:38:04.304Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Currency_Creek_2018-04-04/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/Currency_Creek_2018-04-04/metadata.json
---

Currency Creek Arboretum, Adelaide, managed by Dean Nicolle.
