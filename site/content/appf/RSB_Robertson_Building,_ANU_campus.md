---
title: RN Robertson greenhouse facility
summary: |-
  RN Robertson greenhouse facility, Research School of Biology, ANU.
draft: false
files_local_url: d41d8cd98f00b204e9800998ecf8427erobertson_gh_group1_densified_point_cloud.las
download_url: d41d8cd98f00b204e9800998ecf8427erobertson_gh_group1_densified_point_cloud.las
timestamp: "2018-08-10T16:39:23.800Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/RSB_Robertson_Building,_ANU_campus/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/RSB_Robertson_Building,_ANU_campus/metadata.json
---

Scan of the RN Robertson greenhouse area.
Note, drone flight was done within legal flight regulations. Flight took place outside normal work hours when there were no people present in the flight zone.
