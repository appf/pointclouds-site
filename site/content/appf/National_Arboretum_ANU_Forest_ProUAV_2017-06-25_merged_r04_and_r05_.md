---
title: 'ANU Forest - National Arboretum - ProUAV [2017-06-25] merged r04 and r05'
summary: Description
draft: false
hidden: true
location: National Arboretum, Canberra, ACT, Australia
spp: Corymbia maculata (spotted gum), Eucalyptus tricarpa (red ironbark)
files_local_url: c2215f83061c58e407216397f19d48e22017-06-25-nac-anuf-prouav-f01-r04-r05-merged_group1_densified_point_cloud.ply
download_url: c2215f83061c58e407216397f19d48e22017-06-25-nac-anuf-prouav-f01-r04-r05-merged_group1_densified_point_cloud.ply
timestamp: "2017-09-07T04:21:25.342Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/National_Arboretum_ANU_Forest_ProUAV_2017-06-25_merged_r04_and_r05_/cloud.js
---
