---
title: ANU Research Forest - National Arboretum - ProUAV [2017-10-04]
summary: Description
draft: false
hidden: true
location: National Arboretum, Canberra, ACT, Australia
files_local_url: eb3655c4033f1e97ee2c5f5b53e515f72017-10-04-nac-anuf-prouav-f60m-f80m-r01_group1_densified_point_cloud.ply
download_url: eb3655c4033f1e97ee2c5f5b53e515f72017-10-04-nac-anuf-prouav-f60m-f80m-r01_group1_densified_point_cloud.ply
timestamp: "2017-09-07T04:21:25.342Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/National_Arboretum_ANU_Research_Forest_ProUAV_2017-10-04/cloud.js
---
