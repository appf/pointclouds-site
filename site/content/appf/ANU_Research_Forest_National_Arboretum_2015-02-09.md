---
title: ANU Research Forest - National Arboretum [2015-02-09]
summary: Description
draft: false
hidden: true
location: National Arboretum, Canberra, ACT, Australia
spp: ""
files_local_url: f5393a25a104bc2f779c2e3a90e4e3822015-02-09-nac-anuf-prouav-f01-f02-r01_group1_densified_point_cloud.ply
download_url: f5393a25a104bc2f779c2e3a90e4e3822015-02-09-nac-anuf-prouav-f01-f02-r01_group1_densified_point_cloud.ply
timestamp: "2017-04-19T07:27:51.522Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/ANU_Research_Forest_National_Arboretum_2015-02-09/cloud.js
---
