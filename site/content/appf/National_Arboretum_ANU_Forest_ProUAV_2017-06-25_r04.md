---
title: ANU Forest - National Arboretum - ProUAV [2017-06-25] r04
summary: Description
draft: false
hidden: true
location: National Arboretum, Canberra, ACT, Australia
spp: Corymbia maculata (spotted gum), Eucalyptus tricarpa (red ironbark)
files_local_url: 9e296a2cb9a2a786e63699ea314f2f762017-06-25-nac-anuf-prouav-f01-r04_group1_densified_point_cloud.ply
download_url: 9e296a2cb9a2a786e63699ea314f2f762017-06-25-nac-anuf-prouav-f01-r04_group1_densified_point_cloud.ply
timestamp: "2017-09-07T04:21:25.342Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/National_Arboretum_ANU_Forest_ProUAV_2017-06-25_r04/cloud.js
---
