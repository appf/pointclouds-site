---
title: National Arboretum - ANU Research Forest [2016-12-03]
summary: Scan of the ANU research forest at the National Arboretum
draft: true
location: National Arboretum, Canberra, ACT, Australia
spp: ""
files_local_url: 2181c70bef2196491bce71ef44460a982016-12-03-nac-anuf-prouav-f01-r01_group1_densified_point_cloud.ply
download_url: 2181c70bef2196491bce71ef44460a982016-12-03-nac-anuf-prouav-f01-r01_group1_densified_point_cloud.ply
timestamp: "2017-04-19T07:27:51.522Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/ANU_Research_Forest_National_Arboretum_2016-12-03/cloud.js
---
