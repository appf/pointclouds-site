---
title: National Arboretum - ANU Research Forest [2015-12-03]
summary: Summer scan of the ANU research forest at the National Arboretum
draft: false
location: National Arboretum, Canberra, ACT, Australia
spp: ""
files_local_url: 892f4b6687a4ad95564bf31a124127ad2015-12-03_12-12-nac-anuf-f01-merged_group1_densified_point_cloud_part_1.ply
download_url: 892f4b6687a4ad95564bf31a124127ad2015-12-03_12-12-nac-anuf-f01-merged_group1_densified_point_cloud_part_1.ply
timestamp: "2017-04-19T07:27:51.522Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/National_Arboretum_ANU_Research_Forest_2015-12-03/cloud.js
---
