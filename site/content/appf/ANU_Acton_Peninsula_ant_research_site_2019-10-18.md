---
title: ANU Acton Peninsula ant research site aerial scan
summary: |
  Ant VR drone site scan with the Zeil Lab, ANU
draft: false
files_local_url: c24a8dcfbca1bce08c9c52185220f9fd.las
download_url: 1571626006.1228132_2019-10-18-acton-peninsula-merged.las
timestamp: "2019-10-21T02:58:55.127Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/ANU_Acton_Peninsula_ant_research_site_2019-10-18/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/ANU_Acton_Peninsula_ant_research_site_2019-10-18/metadata.json
---

Merged data from DJI Phantom and Parrot Anafi flights of the Acton Peninsula, near the National Museum. This site is used for insect vision research with the ANU Zeil Lab.
