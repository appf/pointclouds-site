---
title: ANU CEF Growcapsule Site [2017-10-27]
summary: Description
draft: false
hidden: true
files_local_url: 1b1a0e24a62cf3a96ac39d3eadb90dbeMission_1_group1_densified_point_cloud.las
download_url: 1b1a0e24a62cf3a96ac39d3eadb90dbeMission_1_group1_densified_point_cloud.las
timestamp: "2017-10-25T14:37:55.646Z"
display_use_edl: true
display_material: rgb
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/ANU_CEF_Growcapsule_Site_2017-10-27/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/ANU_CEF_Growcapsule_Site_2017-10-27/metadata.json
---
