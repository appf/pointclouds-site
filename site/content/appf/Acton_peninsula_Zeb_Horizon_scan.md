---
title: ANU Acton Peninsula ant research site terrestrial LIDAR scan
summary: Ant VR Zeb Horizon LIDAR site scan with HRPPC and the Zeil Lab
draft: false
files_local_url: 0194bac10c501b21cc480271622be281.las
download_url: 1583467244.6934037_11_ant_vr.las
timestamp: "2020-03-06T04:03:21.352Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/Acton_peninsula_Zeb_Horizon_scan/cloud.js
    settings:
        edlenabled: true
        fov: 50
        usehq: true
        pointsize: 1
        position: -13.697, -21.563, 11.319
        target: 10.704, 13.249, 0.748
---

Zeb Horizon LIDAR scan of the Acton Peninsula, near the National Museum. This site is used for insect vision research with the ANU Zeil Lab.
