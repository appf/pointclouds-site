---
title: National Arboretum - ANU Research Forest [2018-11-30]
summary: Scan of the ANU research forest at the National Arboretum
draft: false
location: National Arboretum, Canberra, ACT, Australia
files_local_url: d41d8cd98f00b204e9800998ecf8427e2018-11-30-nac-anuf-tb_group1_densified_point_cloud.laz
download_url: d41d8cd98f00b204e9800998ecf8427e2018-11-30-nac-anuf-tb_group1_densified_point_cloud.laz
timestamp: "2018-12-01T09:59:35.170Z"
display_material: rgb
display_navigation: orbit
display_use_edl: false
display_fov: 80
display_point_size: 0.2
display_point_limit: 3
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/ANU_Research_Forest_National_Arboretum_2018-11-30/cloud.js
    url_v2: https://b2-anu.appf.org.au/file/acacia/pointclouds/ANU_Research_Forest_National_Arboretum_2018-11-30/metadata.json
---

A drone scan of the National Arboretum, taken by Tim Brown with a DJI Phantom 4 and processed using Pix4D.
