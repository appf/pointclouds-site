---
title: ANU Forest - National Arboretum - ProUAV [2017-06-25] r02
summary: Description
draft: false
hidden: true
location: National Arboretum, Canberra, ACT, Australia
spp: Corymbia maculata (spotted gum), Eucalyptus tricarpa (red ironbark)
files_local_url: 86451f6906d5c252290844519de3f9342017-06-25-nac-anuf-prouav-f01-r02_group1_densified_point_cloud_-_cropped.ply
download_url: 86451f6906d5c252290844519de3f9342017-06-25-nac-anuf-prouav-f01-r02_group1_densified_point_cloud_-_cropped.ply
timestamp: "2017-09-07T04:21:25.342Z"
potree:
    url: https://b2-anu.appf.org.au/file/acacia/pointclouds/National_Arboretum_ANU_Forest_ProUAV_2017-06-25_r02/cloud.js
---
