---
title: "{{ .Name | humanize | title }}"
date: {{ .Date }}
draft: true
hidden: true
summary: ""
potree:
    url: "{{ getenv "PUBLIC_URL_PREFIX" }}/{{ .Name }}/cloud.js"
    url_2: "{{ getenv "PUBLIC_URL_PREFIX" }}/{{ .Name }}/metadata.json"
---

